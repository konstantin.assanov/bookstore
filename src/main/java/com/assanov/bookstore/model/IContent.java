package com.assanov.bookstore.model;

import java.util.List;

public interface IContent {

    String getTitle();

    String getIsbn();

    List<String> getAuthors();

}
