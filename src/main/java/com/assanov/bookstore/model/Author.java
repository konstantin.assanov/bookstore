package com.assanov.bookstore.model;

import java.util.StringJoiner;

public class Author {

    private final String email;
    private final String firstName;
    private final String surname;

    public Author(String email,
                  String firstName,
                  String surname) {
        this.email = email;
        this.firstName = firstName;
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(" | ");
        joiner.add(email)
                .add(firstName)
                .add(surname);

        return joiner.toString();
    }

}
