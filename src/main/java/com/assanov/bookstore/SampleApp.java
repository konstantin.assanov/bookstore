package com.assanov.bookstore;

import com.assanov.bookstore.datasource.CsvFileDataSource;
import com.assanov.bookstore.datasource.DataSourceConfig;
import com.assanov.bookstore.datasource.IDataSource;
import com.assanov.bookstore.model.Author;
import com.assanov.bookstore.model.Book;
import com.assanov.bookstore.model.IContent;
import com.assanov.bookstore.model.Magazine;
import com.assanov.bookstore.orm.deserializers.AuthorMapper;
import com.assanov.bookstore.orm.deserializers.BookMapper;
import com.assanov.bookstore.orm.deserializers.MagazineMapper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Stream;

public class SampleApp {

    private DataSourceConfig config = new DataSourceConfig(
            this.getClass().getResource("/data/").getPath(),
            ";",
            true);

    private IDataSource authors = new CsvFileDataSource(config, "autoren.csv" );
    private IDataSource books = new CsvFileDataSource(config, "buecher.csv");
    private IDataSource magazines = new CsvFileDataSource(config, "zeitschriften.csv");

    private AuthorMapper authorMapper = new AuthorMapper();
    private BookMapper bookMapper = new BookMapper(",");
    private MagazineMapper magazineMapper = new MagazineMapper(",");

    /* list all */

    Stream<Author> findAllAuthors() throws FileNotFoundException {
        return authors.open().load().map(authorMapper::toModel);
    }

    Stream<Book> findAllBooks() throws FileNotFoundException {
        return books.open().load().map(bookMapper::toModel);
    }

    Stream<Magazine> findAllMagazines() throws FileNotFoundException {
        return magazines.open().load().map(magazineMapper::toModel);
    }

    /* find by ISBM */

    Stream<Book> findAllBooksByIsbn(String isbn) throws FileNotFoundException {
        return books
                .open()
                .load()
                .map(bookMapper::toModel)
                .filter(data -> Objects.equals(isbn, data.getIsbn())); //isIsbnEqual(isbn, data.getIsbn()));
    }

    Stream<Magazine> findAllMagazinesByIsbn(String isbn) throws FileNotFoundException {
        return magazines
                .open()
                .load()
                .map(magazineMapper::toModel)
                .filter(data -> Objects.equals(isbn, data.getIsbn())); //isIsbnEqual(isbn, data.getIsbn()));
    }

    // find by Authors

    Stream<Book> findAllBooksByAuthor(String author) throws FileNotFoundException {
        return books
                .open()
                .load()
                .map(bookMapper::toModel)
                .filter(data -> data.getAuthors().contains(author));
    }

    Stream<Magazine> findAllMagazinesByAuthor(String author) throws FileNotFoundException {
        return magazines
                .open()
                .load()
                .map(magazineMapper::toModel)
                .filter(data -> data.getAuthors().contains(author));
    }

    // find all sorted by title

    Stream<Book> findAllBooksOrderedByTitle() throws FileNotFoundException {
        return books
                .open()
                .load()
                .map(bookMapper::toModel)
                .sorted(Comparator.comparing(IContent::getTitle));
    }

    Stream<Magazine> findAllMagazinesOrderedByTitle() throws FileNotFoundException {
        return magazines
                .open()
                .load()
                .map(magazineMapper::toModel)
                .sorted(Comparator.comparing(IContent::getTitle));
    }

    // close data sources

    void closeAuthors() throws IOException {
        authors.close();
    }

    void closeBooks() throws IOException {
        books.close();
    }

    void closeMagazines() throws IOException {
        magazines.close();
    }

    private static void header(String header) {
        System.out.println();
        System.out.println(header);
        System.out.println("==========================================================================");
    }

    public static void main(String[] args) {

        SampleApp app = new SampleApp();

        try {
            header("All Authros");
            app.findAllAuthors().forEach(System.out::println);

            header("All Books");
            app.findAllBooks().forEach(System.out::println);

            header("All Magazines");
            app.findAllMagazines().forEach(System.out::println);

            String bookIsbn = "4545-8558-3232";
            header("All Books by ISBN: " + bookIsbn);
            app.findAllBooksByIsbn(bookIsbn).forEach(System.out::println);

            String magazineIsbn = "2547-8548-2541";
            header("All Magazines by ISBN: " + magazineIsbn);
            app.findAllMagazinesByIsbn(magazineIsbn).forEach(System.out::println);


            String author = "pr-walter@optivo.de";
            header("All Books by Author: " + author);
            app.findAllBooksByAuthor(author).forEach(System.out::println);

            header("All Magazines by Author: " + author);
            app.findAllMagazinesByAuthor(author).forEach(System.out::println);


            header("All Books sorted by Title");
            app.findAllBooksOrderedByTitle().forEach(System.out::println);

            header("All Magazines by Title");
            app.findAllMagazinesOrderedByTitle().forEach(System.out::println);

            app.closeAuthors();
            app.closeBooks();
            app.closeMagazines();

        }
        catch (FileNotFoundException fnfe) {
            System.out.println(fnfe.getMessage());
        }
        catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }

    }

}