package com.assanov.bookstore.datasource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.stream.Stream;

public interface IDataSource {

    IDataSource open() throws FileNotFoundException;

    Stream<String[]> load();

    void close() throws IOException;

}
