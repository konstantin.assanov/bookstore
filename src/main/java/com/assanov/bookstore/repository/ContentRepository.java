package com.assanov.bookstore.repository;

import com.assanov.bookstore.model.IContent;
import com.assanov.bookstore.orm.IDataAdapter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Stream;

public class ContentRepository<T extends IContent> extends Repository<T> {

    public ContentRepository(IDataAdapter<T> dataAdapter, String name) {
        super(dataAdapter, name);
    }

    public Stream<T> findAllByIsbn(String isbn) {
        return source()
                .filter(data -> Objects.equals(isbn, data.getIsbn()));
    }

    public Stream<T> findAllByAuthor(String author) {
        return source()
                .filter(data -> data.getAuthors().contains(author));
    }

    public Stream<T> listAllSortedByTitle() {
        return source()
                .sorted(Comparator.comparing(IContent::getTitle));
    }

    @Override
    public ContentRepository<T> connect() throws FileNotFoundException, IOException {
        super.connect();
        return this;
    }

}