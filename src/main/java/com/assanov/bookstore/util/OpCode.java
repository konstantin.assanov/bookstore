package com.assanov.bookstore.util;

public enum OpCode {
    LIST("list"),
    BY_ISBN("by_isbn"),
    BY_AUTHOR("by_author"),
    SORT("sort");

    private final String value;

    OpCode(String value) {
        this.value = value;
    }

    public String getCode() {
        return value;
    }

    public static OpCode byCode(String value) {
        for(OpCode opCode : values()) {
            if(opCode.value.equals(value)) {
                return opCode;
            }
        }
        return null;
    }

}