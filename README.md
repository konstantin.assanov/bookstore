# CSV Book Store

## Functionalities

- Read all data from multiple CSV files in the "data" folder.
- Return all books / magazines with all details
- Find and return a book / magazine using an ISBN number
- Find and return all the books / magazines of an author
- Sort and return all books / magazines by title

## Overview

There are 2 implementations available in this project:

1. **SampleApp** is more straightforward, simplified implementation, using the packages:<br>
    - **datasource**,<br>
    - **model**,<br>
    - **orm.deserializers**.<br>

**SampleApp.main()** is executed without parameters: it executes all queries and print the result at *stdout*.

    java -classpath target/classes com.assanov.bookstore.SampleApp

2. **BookStoreApp** is more strictly designed, more elaborated repository-like approach, including also:
configuration, adapters (streamed and cached), repository, builders and more elaborated **main()** user interface.

**BookStoreApp.main()** cf. [**BookStoreApp** user interface](#bookstoreapp-user-interface)

## Packages

**config** - contains **Configuration** class, defining the Data Source file paths, and delimiters<br>
**datasource** - driver to read from csv files<br>
**db** - for united access to a set of available repositories<br>
**model** - entities (author, book, magazine)<br>
**orm** - adapters to read from **datasource** and provide higher layer with entities<br>
**orm.deserializers** - mappers to deserialize the entities from the string collections<br>
**repository** - repositories to execute the data queries<br>
**util** - utils for the user interface **BookStoreApp** class<br>

## Design

Code is organized following the clean architecture principles with IoC, where the lower layer's concrete classes
are implementing the interfaces, which then are used for dependency injection. Dependency injection is done via
the constructors.<br>

Configuration parameters are set via a single **Configuration** class, defined in **configuration** package.

The assembly of the application is done with help of the builders: **repository.RepositoryFactory**
and **db.Bookstore.Builder**.<br>

Layers are as follows:

1/ Frameworks & Drivers: **datasource** package

2/ Interface Adapters: **orm** package

3/ Use cases: **repository** package

4/ Entities: **model** package

## Streamed vs Cached processing method

There are 2 processing methods provided:<br>

1/ Streamed: direct data streaming from data source to the terminal operation by Client.<br>
Each new Client's query will re-read the data from data source.<br>
The data source should be closed explicitly by Client.<br>

2/ Cached: first, data is streamed into a simple repository's collection, then, the Client's queries are executed directly
from the repository's cache.<br>
New Client's query will read from cache, without requesting the data source.<br>
The data source is closed automatically after the cache update.<br>

**orm.StreamedAdapter** implements the transparent streaming.<br>
**orm.CachedAdapter** implements the cached processing.<br>

**Streamed** usage:

~~~
    repository
        .connect()                          // open connection to data source & map data to model
        .listAllSortedByTitle()             // execute query
        .forEach(System.out::println);      // terminal operation by client

    repository.close();                     // data source is to be closed by client explicitly
~~~

**Cached** usage:

~~~
    repository
        .connect()                          // load data from the data source, map to model, store in internal cache, and close the data source
        .listAllSortedByTitle()             // execute query on cached data
        .forEach(System.out::println);      // terminal operation by client

    //repository.close();                   // not needed because the data source has been closed already automatically
~~~

## Data Mappers (orm.deserializers)

1. Result Sets are mapped to the model's fields in order of definition in the data source.<br>
  For more of flexibility, later, the mapping could be implemented by the column names.<br>
2. Data Mappers are resistant to invalid data:<br>
    - All Model's field are of string type,<br>
    - Null for any Model's field is accepted,<br>
    - Any number of columns in data source is accepted.<br>

## Configuration & Construction

1/ Configuration class, covering such parameters as Data Source path, file names, and delimiters,
is defined in **configuration** package, and is built with help of **Configuration.Builder**.

2/ Construction of the application is done with the help of the builders<br>
of **repository.RepositoryFactory** (that build the separate repositories), and<br>
of **db.BookStore.Builder** (that creates the BookStore instance for the given set of repositories).

## BookStore

**db.BookStore** groups the available repositories (author, book, and magazine) together
to provide the main point of entry.<br>
**db.BookStore** is constructed with help of **BookStore.Builder**, that instantiates all repositories,
using **RepositoryFactory**, for the given **Configuration**.

## BookStoreApp user interface

**BookStoreApp.main()** takes as arguments:<br>

    (1) Access type (mandatory).        Possible values: basic | cache | help
    (2) Command (mandatory).            Possible values: list |  by_isbn | by_author | sort
    (3) Table (mandatory).              Possible values: author | book | magazine
    (4) Operation Argument(optional).   Possible values: <isbn> | <author>

**Run BookStoreApp example**:

    java -classpath target/classes com.assanov.bookstore.BookStoreApp cache by_author magazine pr-walter@optivo.de

**util.OpCode** describes the available commands.

## Tests

Following tests are provided (with JUnit 5 and Mockito):

1/ Unit Tests, defined for the classes of the packages:<br>
    - **datasource**,<br>
    - **orm**,<br>
    - **orm.deserializers**, and<br>
    - **repository**.<br>

2/ End-To-End tests: parametrized **BookStoreEnd2EndTest** executes all functionalities for the 'streamed' and 'cached' modes both.

## Out of Scope

1. Advanced features for more efficient data processing: indices, primary keys, *join-* queries, etc.<br>
2. Executable Jar would require a slightly different **CsvResourceDataSource** class to access the resources via *InputStreamReader*.<br>
3. **Configuration** further could be initialized from *application.properties*.
