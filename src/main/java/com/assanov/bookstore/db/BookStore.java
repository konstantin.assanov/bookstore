package com.assanov.bookstore.db;

import com.assanov.bookstore.config.Configuration;
import com.assanov.bookstore.model.Author;
import com.assanov.bookstore.model.Book;
import com.assanov.bookstore.model.IContent;
import com.assanov.bookstore.model.Magazine;
import com.assanov.bookstore.repository.ContentRepository;
import com.assanov.bookstore.repository.RepositoryFactory;
import com.assanov.bookstore.repository.Repository;

import java.util.HashMap;
import java.util.Map;

public class BookStore {

    private final boolean isCached;

    public final Repository<Author> authors;
    public final ContentRepository<Book> books;
    public final ContentRepository<Magazine> magazines;

    public final Map<String, Repository> repositories = new HashMap<>();
    public final Map<String, ContentRepository<? extends IContent>> contentRepositories = new HashMap<>();

    private BookStore(Repository<Author> authors,
                      ContentRepository<Book> books,
                      ContentRepository<Magazine> magazines,
                      boolean isCached) {
        this.isCached = isCached;
        this.authors = authors;
        this.books = books;
        this.magazines = magazines;
        registerAsRepository(authors);
        registerAsContentRepository(books);
        registerAsContentRepository(magazines);
    }

    private void registerAsRepository(Repository repository) {
        repositories.put(repository.getName(), repository);
    }

    private void registerAsContentRepository(ContentRepository<? extends IContent> repository) {
        repositories.put(repository.getName(), repository);
        contentRepositories.put(repository.getName(), repository);

    }

    public boolean isCached() {
        return isCached;
    }

    public static class Builder {

        private boolean isCached = false;
        private Configuration configuration;

        public Builder(Configuration configuration) {
            this.configuration = configuration;
        }

        public Builder setCached(boolean isCached) {
            this.isCached = isCached;
            return this;
        }

        public BookStore build() {
            RepositoryFactory factory = new RepositoryFactory(configuration);
            return new BookStore(
                    factory.authors(isCached),
                    factory.books(isCached),
                    factory.magazines(isCached),
                    isCached);
        }
    }

}