package com.assanov.bookstore.orm;

import com.assanov.bookstore.datasource.IDataSource;
import com.assanov.bookstore.orm.deserializers.IDataMapper;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class CachedAdapterTest {

    @Test
    void shouldHaveEmptyCacheAfterCreation() {
        CachedAdapter adapter = new CachedAdapter(mock(IDataSource.class), mock(IDataMapper.class));

        assertEquals(0, adapter.source().count());
    }

    @Test
    void shouldFillCacheSuccessfullyOnOpen() throws FileNotFoundException, IOException {
        List<String[]> loaded = List.of(new String[]{"1"}, new String[]{"2"}, new String[]{"3"});
        List<String> models = List.of("Model #1", "Model #2", "Model #3");

        IDataSource dataSource = mock(IDataSource.class);
        when(dataSource.open()).thenReturn(dataSource);
        when(dataSource.load()).thenReturn(loaded.stream());

        IDataMapper<String> dataMapper = mock(IDataMapper.class);
        for (int i = 0; i < loaded.size(); i++) {
            when(dataMapper.toModel(eq(loaded.get(i)))).thenReturn(models.get(i));
        }

        CachedAdapter<String> adapter = new CachedAdapter(dataSource, dataMapper);

        adapter.open();

        List<String> inCache = adapter.source().collect(toList());

        assertEquals(3, inCache.size());
        assertEquals(models, inCache);

        verify(dataSource).close();
    }

}
