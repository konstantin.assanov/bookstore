package com.assanov.bookstore.repository;

import com.assanov.bookstore.orm.IDataAdapter;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RepositoryTest {

    @Test
    void shouldSetRepositoryNameSuccessfully() {
        String tableName = "Table Name";

        Repository repository = new Repository(mock(IDataAdapter.class), tableName);

        assertEquals(tableName, repository.getName());
    }

    @Test
    void shouldReturnStreamFromDataAdapterWhenFindAll() {
        String tableName = "Table Name";
        IDataAdapter dataAdapter = mock(IDataAdapter.class);

        Stream data = mock(Stream.class);

        when(dataAdapter.source()).thenReturn(data);

        Repository repository = new Repository(dataAdapter, tableName);

        assertEquals(data, repository.findAll());
    }

}