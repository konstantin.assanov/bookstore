package com.assanov.bookstore.repository;

import com.assanov.bookstore.model.IContent;
import com.assanov.bookstore.orm.IDataAdapter;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ContentRepositoryTest {

    @Test
    void shouldFindByIsbnSuccessfully() {
        String tableName = "Table Name";
        IDataAdapter<IContent> dataAdapter = mock(IDataAdapter.class);
        List<IContent> elements = List.of(
                mock(IContent.class),
                mock(IContent.class),
                mock(IContent.class));

        when(elements.get(0).getIsbn()).thenReturn("12-12");
        when(elements.get(1).getIsbn()).thenReturn("80-90");
        when(elements.get(2).getIsbn()).thenReturn("100-1");

        when(dataAdapter.source()).thenReturn(elements.stream());

        ContentRepository<IContent> repository = new ContentRepository<>(dataAdapter, tableName);

        Optional<IContent> found = repository.findAllByIsbn("80-90").findFirst();

        assertTrue(found.isPresent());
        assertEquals(elements.get(1), found.get());
    }

    @Test
    void shouldFindByAuthorSuccessfully() {
        String tableName = "Table Name";
        IDataAdapter<IContent> dataAdapter = mock(IDataAdapter.class);
        List<IContent> elements = List.of(
                mock(IContent.class),
                mock(IContent.class),
                mock(IContent.class));

        when(elements.get(0).getAuthors()).thenReturn(List.of("Balzac"));
        when(elements.get(1).getAuthors()).thenReturn(List.of("Wilde", "Fitzgerald"));
        when(elements.get(2).getAuthors()).thenReturn(List.of("London", "Wilde"));

        when(dataAdapter.source()).thenReturn(elements.stream());

        ContentRepository<IContent> repository = new ContentRepository<>(dataAdapter, tableName);

        List<IContent> found = repository.findAllByAuthor("Wilde").collect(Collectors.toList());

        assertEquals(2, found.size());
        assertTrue(found.containsAll(List.of(elements.get(1), elements.get(2))));
    }

    @Test
    void shouldReturnSortedByTitleWhenSort() {
        String tableName = "Table Name";
        IDataAdapter<IContent> dataAdapter = mock(IDataAdapter.class);
        List<IContent> elements = List.of(
                mock(IContent.class),
                mock(IContent.class),
                mock(IContent.class));

        when(elements.get(0).getTitle()).thenReturn("Rolf");
        when(elements.get(1).getTitle()).thenReturn("Antoine");
        when(elements.get(2).getTitle()).thenReturn("Manuel");

        when(dataAdapter.source()).thenReturn(elements.stream());

        ContentRepository<IContent> repository = new ContentRepository<>(dataAdapter, tableName);

        List<IContent> sorted = repository.listAllSortedByTitle().collect(Collectors.toList());

        assertEquals(3, sorted.size());
        assertEquals(elements.get(1), sorted.get(0));
        assertEquals(elements.get(2), sorted.get(1));
        assertEquals(elements.get(0), sorted.get(2));
    }

}