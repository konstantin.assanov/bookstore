package com.assanov.bookstore.model;

import java.util.List;
import java.util.StringJoiner;

public class Magazine implements IContent {
    final String title;
    final String isbn;
    final List<String> authors;
    final String dateOfPublication;

    public Magazine(String title,
                    String isbn,
                    String[] authors,
                    String dateOfPublication) {
        this.title = title;
        this.isbn = isbn;
        this.authors = List.of(authors);
        this.dateOfPublication = dateOfPublication;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getIsbn() {
        return isbn;
    }

    @Override
    public List<String> getAuthors() {
        return authors;
    }

    public String getDateOfPublication() {
        return dateOfPublication;
    }

    @Override
    public String toString() {
        StringJoiner authorJoiner = new StringJoiner(",");
        authors.forEach(authorJoiner::add);

        StringJoiner joiner = new StringJoiner(" | ");
        joiner.add(title)
                .add(isbn)
                .add(authorJoiner.toString())
                .add(dateOfPublication);

        return joiner.toString();
    }

}
