package com.assanov.bookstore.orm.deserializers;

import com.assanov.bookstore.model.Book;

public class BookMapper implements IDataMapper<Book> {

    private final String listDelimiter;

    public BookMapper(String listDelimiter) {
        this.listDelimiter = listDelimiter;
    }

    @Override
    public Book toModel(String[] fields) {
        return new Book(
                get(fields, 0),
                get(fields, 1),
                split(get(fields, 2), listDelimiter),
                get(fields, 3));
    }

}