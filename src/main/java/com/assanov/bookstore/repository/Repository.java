package com.assanov.bookstore.repository;

import com.assanov.bookstore.orm.IDataAdapter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.stream.Stream;

public class Repository<T> {

    private final String name;
    protected final IDataAdapter<T> dataAdapter;

    public Repository(IDataAdapter<T> dataAdapter, String name) {
        this.dataAdapter = dataAdapter;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Repository<T> connect() throws FileNotFoundException, IOException {
        dataAdapter.open();
        return this;
    }

    public void close() throws IOException {
        dataAdapter.close();
    }

    public Stream<T> findAll() {
        return source();
    }

    protected Stream<T> source() {
        return dataAdapter.source();
    }

}