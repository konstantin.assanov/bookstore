package com.assanov.bookstore.orm.deserializers;

import com.assanov.bookstore.model.Magazine;

public class MagazineMapper implements IDataMapper<Magazine> {

    private final String listDelimiter;

    public MagazineMapper(String listDelimiter) {
        this.listDelimiter = listDelimiter;
    }

    @Override
    public Magazine toModel(String[] fields) {
        return new Magazine(
                get(fields, 0),
                get(fields, 1),
                split(get(fields, 2), listDelimiter),
                get(fields, 3));
    }

}
