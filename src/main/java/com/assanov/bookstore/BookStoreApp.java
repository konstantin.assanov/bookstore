package com.assanov.bookstore;

import com.assanov.bookstore.config.Configuration;
import com.assanov.bookstore.db.BookStore;
import com.assanov.bookstore.model.IContent;
import com.assanov.bookstore.repository.ContentRepository;
import com.assanov.bookstore.repository.Repository;
import com.assanov.bookstore.util.OpCode;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.assanov.bookstore.util.Utils.joinValues;
import static com.assanov.bookstore.util.Utils.safeArg;

public class BookStoreApp {

    private static void execute(BookStore db, String opCodeValue, String table, String arg)
            throws FileNotFoundException, IOException, IllegalArgumentException {
        Repository<?> repo;
        ContentRepository<? extends IContent> contentRepository;
        OpCode opCode = OpCode.byCode(opCodeValue);
        if (opCode == null)
            throw new IllegalArgumentException("Unknown command: <" + opCodeValue + ">");

        switch (opCode) {
            case LIST:
                repo = repository(db, table);
                repo.connect()
                    .findAll()
                    .forEach(System.out::println);

                if (!db.isCached())
                    repo.close();
                break;
            case BY_ISBN:
                contentRepository = contentRepository(db, table);
                contentRepository
                    .connect()
                    .findAllByIsbn(arg)
                    .forEach(System.out::println);

                if (!db.isCached())
                    contentRepository.close();
                break;
            case BY_AUTHOR:
                contentRepository = contentRepository(db, table);
                contentRepository
                    .connect()
                    .findAllByAuthor(arg)
                    .forEach(System.out::println);

                if (!db.isCached())
                    contentRepository.close();
                break;
            case SORT:
                contentRepository = contentRepository(db, table);
                contentRepository
                    .connect()
                    .listAllSortedByTitle()
                    .forEach(System.out::println);

                if (!db.isCached())
                    contentRepository.close();
                break;
            default:
                throw new IllegalArgumentException("Unprocessed command: <" + opCode + ">");
        }

    }

    private static Repository repository(BookStore db, String table) {
        return Optional.ofNullable(db.repositories.get(table))
                .orElseThrow(() -> new IllegalArgumentException("Unknown table: <" + table + ">"));
    }

    private static ContentRepository<? extends IContent> contentRepository(BookStore db, String table) {
        return Optional.ofNullable(db.contentRepositories.get(table))
                .orElseThrow(() -> new IllegalArgumentException("Unknown table: <" + table + ">"));
    }

    private static void printHelp(Collection<String> tables) {
        System.out.println("Valid arguments:");
        String opCodes = joinValues(Stream.of(OpCode.values()).map(OpCode::getCode).collect(Collectors.toList()));
        System.out.println(" (1) Access type (mandatory).   Possible values: basic, cache, help");
        System.out.println(" (2) Command (mandatory).       Possible values: " + opCodes);
        System.out.println(" (3) Table (mandatory).         Possible values: " + joinValues(tables));
        System.out.println(" (4) Operation Argument(optional). Possible values: ");
        System.out.println("\t\t For command " + OpCode.BY_ISBN.getCode() +": <isbn>");
        System.out.println("\t\t For command " + OpCode.BY_AUTHOR.getCode() +": <author>");
    }

    public static void main(String[] args) {

        Configuration configuration;
        try {
            configuration = new Configuration.Builder()
                    .setDataSourceDirectoryinResources("/data")
                    .addTable("author", "autoren.csv")
                    .addTable("book", "buecher.csv")
                    .addTable("magazine", "zeitschriften.csv")
                    .build();
        }
        catch (Exception e) {
            System.out.println("Invalid configuration. " + e.getMessage());
            return;
        }

        if ("help".equals(safeArg(args, 0))) {
            printHelp(configuration.tables.keySet());
            return;
        }

        boolean isBasic = "basic".equals(safeArg(args, 0));
        String opCode = safeArg(args, 1);
        String tableName = safeArg(args, 2);
        String arg = safeArg(args, 3);

        BookStore.Builder builder = new BookStore.Builder(configuration);

        BookStore db = builder.setCached(!isBasic).build();
        Set<String> tables = db.repositories.keySet();

        try {
            execute(db, opCode, tableName, arg);
        }
        catch (IllegalArgumentException iae) {
            System.out.println("Operation could not be executed. " + iae.getMessage());
            printHelp(tables);
        }
        catch (FileNotFoundException fnfe) {
            System.out.println("Failed to connect to the table: " + fnfe.getMessage());
        }
        catch (IOException ioe) {
            System.out.println("Failed I/O: " + ioe.getMessage());
        }

    }

}