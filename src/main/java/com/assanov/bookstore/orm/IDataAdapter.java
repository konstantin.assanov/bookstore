package com.assanov.bookstore.orm;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.stream.Stream;

public interface IDataAdapter<T> {

    IDataAdapter<T> open() throws FileNotFoundException, IOException;

    Stream<T> source();

    void close() throws IOException;

}