package com.assanov.bookstore.datasource;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Stream;

public class CsvFileDataSource implements IDataSource {

    private final DataSourceConfig config;
    private final String filename;
    private BufferedReader reader;

    public CsvFileDataSource(DataSourceConfig config, String filename) {
        this.config = config;
        this.filename = filename;
    }

    @Override
    public IDataSource open() throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(config.path + "/" + filename));
        return this;
    }


    @Override
    public Stream<String[]> load() {
        return reader
                .lines()
                .skip(config.isSkipHeader ? 1 : 0)
                .map(line -> line.split(config.delimiter));
    }

    @Override
    public void close() throws IOException {
        if (reader != null) {
            reader.close();
        }
    }

}
