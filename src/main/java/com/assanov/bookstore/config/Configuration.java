package com.assanov.bookstore.config;

import com.assanov.bookstore.datasource.DataSourceConfig;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Configuration {

    public final DataSourceConfig dataSourceConfig;
    public final Map<String, String> tables;
    public final String listDelimiter;

    private Configuration(DataSourceConfig dataSourceConfig, Map<String, String> tables, String listDelimiter) {
        this.dataSourceConfig = dataSourceConfig;
        this.tables = tables;
        this.listDelimiter = listDelimiter;
    }

    public static class Builder {

        private String path;
        private String rowDelimiter = ";";
        private boolean isSkipHeader = true;

        private final Map<String, String> tables = new HashMap<>();

        private String listDelimiter = ",";

        public Builder setDataSourceDirectory(String path) {
            this.path = path;
            return this;
        }

        public Builder setDataSourceDirectoryinResources(String dirInResources) {
            final URL url = this.getClass().getResource(dirInResources);
            if (url == null)
                throw new IllegalArgumentException("Resource not found: <" + dirInResources + ">");

            this.path = url.getPath();
            return this;
        }

        public Builder setRowDelimiter(String rowDelimiter) {
            this.rowDelimiter = rowDelimiter;
            return this;
        }

        public Builder skipHeader(boolean isSkipHeader) {
            this.isSkipHeader = isSkipHeader;
            return this;
        }

        public Builder addTable(String tableName, String fileName) {
            tables.put(tableName, fileName);
            return this;
        }

        public Builder setListDelimiter(String listDelimiter) {
            this.listDelimiter = listDelimiter;
            return this;
        }

        public Configuration build() {
            return new Configuration(
                    new DataSourceConfig(path, rowDelimiter, isSkipHeader),
                    tables,
                    listDelimiter);
        }

    }

}