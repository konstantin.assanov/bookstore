package com.assanov.bookstore.datasource;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CsvFileDataSourceTest {

    private final List<String[]> data = List.of(
            new String[]{"Header#1", "Header#2"},
            new String[]{"A1", "A2"},
            new String[]{"B1", "B2"},
            new String[]{"C1", "C2"});

    @Test
    void shouldThrowFildeNotFoundExceptionIfInvalidPath() {
        DataSourceConfig dataSourceConfig = new DataSourceConfig("", ";", true);

        CsvFileDataSource dataSource = new CsvFileDataSource(dataSourceConfig, "");
        Assertions.assertThrows(FileNotFoundException.class, () ->
            dataSource.open()
        );
    }

    @Test
    void shouldCreateReaderSuccessfully() throws IOException {
        File tempFile = File.createTempFile("author", ".csv");

        DataSourceConfig dataSourceConfig = new DataSourceConfig(
                tempFile.getParent(), ";", true);

        CsvFileDataSource dataSource = new CsvFileDataSource(dataSourceConfig, tempFile.getName());

        Assertions.assertDoesNotThrow(() ->
            dataSource.open()
        );

        tempFile.delete();
    }

    private File buildTempFile(String prefix, String suffix, List<String[]> fileContent, String delimiter) throws IOException {
        File tempFile = File.createTempFile(prefix, suffix);

        List<String> lines = fileContent.stream().map(cells -> {
            StringJoiner joiner = new StringJoiner(delimiter);
            List.of(cells).forEach(joiner::add);
            return joiner.toString();
        }).collect(Collectors.toList());

        Files.write(tempFile.toPath(), lines, StandardOpenOption.APPEND);

        return tempFile;
    }

    @Test
    void shouldLoadAndParseSuccessfullyFromFileWithoutSkippingFirstRow() throws IOException {
        String rowDelimiter = ";";
        File tempFile = buildTempFile("author", ".csv", data, rowDelimiter);

        DataSourceConfig dataSourceConfig = new DataSourceConfig(
                tempFile.getParent(), rowDelimiter, false);

        CsvFileDataSource dataSource = new CsvFileDataSource(dataSourceConfig, tempFile.getName());

        List<String[]> loaded = dataSource.open().load().collect(Collectors.toList());

        assertEquals(data.size(), loaded.size());
        for (int i = 0; i < data.size(); i++)
            assertArrayEquals(data.get(i), loaded.get(i));

        tempFile.delete();
    }

    @Test
    void shouldLoadAndParseSuccessfullyFromFileSkippingFirstRow() throws IOException {
        String rowDelimiter = ";";
        File tempFile = buildTempFile("author", ".csv", data, rowDelimiter);

        DataSourceConfig dataSourceConfig = new DataSourceConfig(
                tempFile.getParent(), rowDelimiter, true);

        CsvFileDataSource dataSource = new CsvFileDataSource(dataSourceConfig, tempFile.getName());

        List<String[]> loaded = dataSource.open().load().collect(Collectors.toList());

        assertEquals(data.size() - 1, loaded.size());
        for (int i = 0; i < data.size() - 1; i++)
            assertArrayEquals(data.get(i + 1), loaded.get(i));

        tempFile.delete();
    }

    @Test
    void shouldNotFailIfClosingAfterCreation() throws IOException {
        String rowDelimiter = ";";
        File tempFile = buildTempFile("author", ".csv", data, rowDelimiter);

        DataSourceConfig dataSourceConfig = new DataSourceConfig(
                tempFile.getParent(), rowDelimiter, true);

        CsvFileDataSource dataSource = new CsvFileDataSource(dataSourceConfig, tempFile.getName());

        Assertions.assertDoesNotThrow(() ->
            dataSource.close()
        );
    }

    @Test
    void shouldNotFailIfClosingTwice() throws IOException {
        String rowDelimiter = ";";
        File tempFile = buildTempFile("author", ".csv", data, rowDelimiter);

        DataSourceConfig dataSourceConfig = new DataSourceConfig(
                tempFile.getParent(), rowDelimiter, true);

        CsvFileDataSource dataSource = new CsvFileDataSource(dataSourceConfig, tempFile.getName());

        long count = dataSource.open().load().count();

        Assertions.assertDoesNotThrow(() -> {
            dataSource.close();
            dataSource.close();
        });
    }

}