package com.assanov.bookstore.orm;

import com.assanov.bookstore.datasource.IDataSource;
import com.assanov.bookstore.orm.deserializers.IDataMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

public class StreamedAdapterTest {

    @Test
    void shouldOpenDataSourceOnOpenCall() throws IOException {
        IDataSource dataSource = mock(IDataSource.class);
        StreamedAdapter adapter = new StreamedAdapter(dataSource, mock(IDataMapper.class));

        adapter.open();

        verify(dataSource).open();
    }

    @Test
    void shouldLoadFromDataSourceSuccessfully() throws IOException {
        List<String[]> loaded = List.of(new String[]{"1"}, new String[]{"2"}, new String[]{"3"});
        List<String> models = List.of("Model #1", "Model #2", "Model #3");

        IDataSource dataSource = mock(IDataSource.class);
        when(dataSource.load()).thenReturn(loaded.stream());

        IDataMapper<String> dataMapper = mock(IDataMapper.class);
        for (int i = 0; i < loaded.size(); i++) {
            when(dataMapper.toModel(eq(loaded.get(i)))).thenReturn(models.get(i));
        }

        StreamedAdapter<String> adapter = new StreamedAdapter(dataSource, dataMapper);

        adapter.open();

        List<String> received = adapter.source().collect(toList());

        assertEquals(3, received.size());
        assertEquals(models, received);

        verify(dataSource, never()).close();

        adapter.close();
    }

    @Test
    void shouldCloseDataSourceOnCloseCall() throws IOException {
        IDataSource dataSource = mock(IDataSource.class);
        StreamedAdapter adapter = new StreamedAdapter(dataSource, mock(IDataMapper.class));

        adapter.close();

        verify(dataSource).close();
    }

}
