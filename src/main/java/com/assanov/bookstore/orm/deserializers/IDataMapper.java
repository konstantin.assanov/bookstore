package com.assanov.bookstore.orm.deserializers;

import java.util.Optional;

public interface IDataMapper<T> {

    T toModel(String[] fields);

    default String get(String[] fields, int index) {
        if (fields == null || index >= fields.length)
            return null;

        return Optional
                .ofNullable(fields[index])
                .map(String::trim)
                .get();
    }

    default String[] split(String field, String listDelimiter) {
        if (field == null || field.isBlank())
            return new String[]{};

        return field
                .trim()
                .split("\\s*" + listDelimiter + "\\s*");
    }

}
