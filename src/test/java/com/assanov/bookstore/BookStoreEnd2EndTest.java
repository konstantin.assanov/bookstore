package com.assanov.bookstore;

import com.assanov.bookstore.config.Configuration;
import com.assanov.bookstore.db.BookStore;
import com.assanov.bookstore.model.Book;
import com.assanov.bookstore.model.Magazine;
import com.assanov.bookstore.repository.Repository;
import org.junit.jupiter.api.AfterEach;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class BookStoreEnd2EndTest {

    Configuration configuration = new Configuration.Builder()
                .setDataSourceDirectoryinResources("/data")
                .addTable("author", "autoren.csv")
                .addTable("book", "buecher.csv")
                .addTable("magazine", "zeitschriften.csv")
                .build();

    BookStore.Builder builder = new BookStore.Builder(configuration);

    BookStore storeStreamed = builder.setCached(false).build();
    BookStore storeCached = builder.setCached(true).build();

    Repository repository;

    private BookStore store(boolean isCached) {
        return isCached ? storeCached : storeStreamed;
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void shouldListAuthorsSuccessfully(boolean isCached) throws IOException {
        repository = store(isCached).authors;

        long nbAuthors = repository.connect()
                .findAll()
                .count();

        assertEquals(6L, nbAuthors);
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void shouldListBooksSuccessfully(boolean isCached) throws IOException {
        repository = store(isCached).books;

        long nbBooks = repository.connect()
                .findAll()
                .count();

        assertEquals(8L, nbBooks);
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void shouldListMagazinesSuccessfully(boolean isCached) throws IOException {
        repository = store(isCached).authors;

        long nbMagazines = repository.connect()
                .findAll()
                .count();

        assertEquals(6L, nbMagazines);
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void shouldFindBookByIsbnSuccessfully(boolean isCached) throws IOException {
        final String isbn = "4545-8558-3232";

        repository = store(isCached).books;

        Optional<Book> bookOpt = store(isCached).books.connect()
                .findAllByIsbn(isbn)
                .findFirst();

        assertTrue(bookOpt.isPresent());

        Book book = bookOpt.get();

        assertEquals(isbn, book.getIsbn());
        assertEquals("Schlank im Schlaf", book.getTitle());
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void shouldFindMagazineByIsbnSuccessfully(boolean isCached) throws IOException {
        final String isbn = "2547-8548-2541";

        repository = store(isCached).magazines;

        Optional<Magazine> magazineOpt = store(isCached).magazines.connect()
                .findAllByIsbn(isbn)
                .findFirst();

        assertTrue(magazineOpt.isPresent());

        Magazine magazine = magazineOpt.get();

        assertEquals(isbn, magazine.getIsbn());
        assertEquals("Der Weinkenner", magazine.getTitle());
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void shouldFindBookByAuthorSuccessfully(boolean isCached) throws IOException {
        final String author = "pr-gustafsson@optivo.de";

        repository = store(isCached).books;

        Optional<Book> bookOpt = store(isCached).books.connect()
                .findAllByAuthor(author)
                .findFirst();

        assertTrue(bookOpt.isPresent());

        Book book = bookOpt.get();

        assertTrue(book.getAuthors().contains(author));
        assertEquals("Schlank im Schlaf", book.getTitle());
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void shouldFindMagazineByAuthorSuccessfully(boolean isCached) throws IOException {
        final String author = "pr-gustafsson@optivo.de";

        repository = store(isCached).magazines;

        Optional<Magazine> magazineOpt = store(isCached).magazines.connect()
                .findAllByAuthor(author)
                .findFirst();

        assertTrue(magazineOpt.isPresent());

        Magazine magazine = magazineOpt.get();

        assertTrue(magazine.getAuthors().contains(author));
        assertEquals("Vinum", magazine.getTitle());
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void shouldSortBooksByTitleSuccessfully(boolean isCached) throws IOException {
        repository = store(isCached).books;

        List<String> titlesSorted = store(isCached).books.connect()
                .listAllSortedByTitle()
                .map(Book::getTitle)
                .collect(toList());

        assertEquals(8, titlesSorted.size());
        assertEquals(List.of("Das Perfekte Dinner. Die besten Rezepte",
                "Das Piratenkochbuch. Ein Spezialit�tenkochbuch mit den 150 leckersten Rezepten",
                "Das gro�e GU-Kochbuch Kochen f�r Kinder",
                "Genial italienisch",
                "Ich helf dir kochen. Das erfolgreiche Universalkochbuch mit gro�em Backteil",
                "O'Reillys Kochbuch f�r Geeks",
                "Schlank im Schlaf",
                "Schuhbecks Kochschule. Kochen lernen mit Alfons Schuhbeck"),
                titlesSorted);
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void shouldSortMagazinesByTitleSuccessfully(boolean isCached) throws IOException {
        repository = store(isCached).magazines;

        List<String> titlesSorted = store(isCached).magazines.connect()
                .listAllSortedByTitle()
                .map(Magazine::getTitle)
                .collect(toList());

        assertEquals(6, titlesSorted.size());
        assertEquals(List.of("Der Weinkenner",
                "Gourmet",
                "Kochen f�r Genie�er",
                "Meine Familie und ich",
                "Sch�ner kochen",
                "Vinum"),
                titlesSorted);
    }

    @AfterEach
    void closeRepository() throws IOException {
        repository.close();
    }

}