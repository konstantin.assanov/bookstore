package com.assanov.bookstore.model;

import java.util.List;
import java.util.StringJoiner;

public class Book implements IContent {

    private final String title;
    private final String isbn;
    private final List<String> authors;
    private final String description;

    public Book(String title,
            String isbn,
            String[] authors,
            String description) {
        this.title = title;
        this.isbn = isbn;
        this.authors = List.of(authors);
        this.description = description;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getIsbn() {
        return isbn;
    }

    @Override
    public List<String> getAuthors() {
        return authors;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        StringJoiner authorJoiner = new StringJoiner(",");
        authors.forEach(authorJoiner::add);

        StringJoiner joiner = new StringJoiner(" | ");
        joiner.add(title)
                .add(isbn)
                .add(authorJoiner.toString())
                .add(description);

        return joiner.toString();
    }

}
