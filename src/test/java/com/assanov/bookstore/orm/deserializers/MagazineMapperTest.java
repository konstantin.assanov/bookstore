package com.assanov.bookstore.orm.deserializers;

import com.assanov.bookstore.model.Magazine;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNull;

public class MagazineMapperTest {

    @Test
    void shouldReturnAuthorSuccessfully() {
        Magazine magazine = new MagazineMapper(",")
                .toModel(new String[]{"title", "isbn", " Author #1 , Author #2   ", "date"});

        assertNotNull(magazine);

        assertEquals("title", magazine.getTitle());
        assertEquals("isbn", magazine.getIsbn());
        assertEquals(List.of("Author #1" , "Author #2"), magazine.getAuthors());
        assertEquals("date", magazine.getDateOfPublication());
    }

    @Test
    void shouldReturnAuthorSuccessfullyIfEmptyResultSet() {
        Magazine magazine = new MagazineMapper(",")
                .toModel(new String[]{});

        assertNotNull(magazine);

        assertNull(magazine.getTitle());
        assertNull(magazine.getIsbn());
        assertNotNull(magazine.getAuthors());
        assertTrue(magazine.getAuthors().isEmpty());
        assertNull(magazine.getDateOfPublication());
    }

}
