package com.assanov.bookstore.repository;

import com.assanov.bookstore.config.Configuration;
import com.assanov.bookstore.datasource.CsvFileDataSource;
import com.assanov.bookstore.datasource.IDataSource;
import com.assanov.bookstore.orm.deserializers.AuthorMapper;
import com.assanov.bookstore.orm.deserializers.BookMapper;
import com.assanov.bookstore.orm.deserializers.IDataMapper;
import com.assanov.bookstore.orm.deserializers.MagazineMapper;
import com.assanov.bookstore.model.Author;
import com.assanov.bookstore.model.Book;
import com.assanov.bookstore.model.Magazine;
import com.assanov.bookstore.orm.CachedAdapter;
import com.assanov.bookstore.orm.IDataAdapter;
import com.assanov.bookstore.orm.StreamedAdapter;

public class RepositoryFactory {

    private final Configuration config;

    public RepositoryFactory(Configuration config) {
        this.config = config;
    }

    public Repository<Author> authors(boolean isCached) {
        final String tableName = "author";
        return new Repository<>(
                adapter(dataSource(config.tables.get(tableName)),
                        new AuthorMapper(),
                        isCached), tableName);
    }

    public ContentRepository<Book> books(boolean isCached) {
        final String tableName = "book";
        return new ContentRepository<>(
                adapter(dataSource(config.tables.get(tableName)),
                        new BookMapper(config.listDelimiter),
                        isCached), tableName);
    }

    public ContentRepository<Magazine> magazines(boolean isCached) {
        final String tableName = "magazine";
        return new ContentRepository<>(
                adapter(dataSource(config.tables.get(tableName)),
                        new MagazineMapper(config.listDelimiter),
                        isCached), tableName);
    }

    private IDataSource dataSource(String filename) {
        return new CsvFileDataSource(config.dataSourceConfig, filename);
    }

    private <T> IDataAdapter<T> adapter(IDataSource dataSource,
                                        IDataMapper<T> dataMapper,
                                        boolean isCached) {
        return isCached
                ? new CachedAdapter<>(dataSource, dataMapper)
                : new StreamedAdapter<>(dataSource, dataMapper);
    }

}