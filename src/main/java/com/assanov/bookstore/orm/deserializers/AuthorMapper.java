package com.assanov.bookstore.orm.deserializers;

import com.assanov.bookstore.model.Author;

public class AuthorMapper implements IDataMapper<Author> {

    @Override
    public Author toModel(String[] fields) {
        return new Author(
                get(fields, 0),
                get(fields, 1),
                get(fields, 2));
    }

}