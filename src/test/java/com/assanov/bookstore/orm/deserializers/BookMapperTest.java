package com.assanov.bookstore.orm.deserializers;

import com.assanov.bookstore.model.Book;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNull;

public class BookMapperTest {

    @Test
    void shouldReturnAuthorSuccessfully() {
        Book book = new BookMapper(",")
                .toModel(new String[]{"title", "isbn", " Author #1 , Author #2   ", "description"});

        assertNotNull(book);

        assertEquals("title", book.getTitle());
        assertEquals("isbn", book.getIsbn());
        assertEquals(List.of("Author #1" , "Author #2"), book.getAuthors());
        assertEquals("description", book.getDescription());
    }

    @Test
    void shouldReturnAuthorSuccessfullyIfEmptyResultSet() {
        Book book = new BookMapper(",")
                .toModel(new String[]{});

        assertNotNull(book);

        assertNull(book.getTitle());
        assertNull(book.getIsbn());
        assertNotNull(book.getAuthors());
        assertTrue(book.getAuthors().isEmpty());
        assertNull(book.getDescription());
    }

}