package com.assanov.bookstore.orm;

import com.assanov.bookstore.orm.deserializers.IDataMapper;
import com.assanov.bookstore.datasource.IDataSource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CachedAdapter<T> implements IDataAdapter<T> {

    private final IDataSource dataSource;
    private final IDataMapper<T> mapper;

    private List<T> cache = List.of();

    public CachedAdapter(IDataSource dataSource, IDataMapper<T> mapper) {
        this.dataSource = dataSource;
        this.mapper = mapper;
    }

    public IDataAdapter<T> open() throws FileNotFoundException, IOException {
        cache = dataSource.open()
                .load()
                .map(mapper::toModel)
                .collect(Collectors.toList());
        dataSource.close();
        return this;
    }

    public Stream<T> source() {
        return cache.stream();
    }

    public void close() throws IOException {
    }

}