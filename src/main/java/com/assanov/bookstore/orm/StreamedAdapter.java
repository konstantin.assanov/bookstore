package com.assanov.bookstore.orm;

import com.assanov.bookstore.orm.deserializers.IDataMapper;
import com.assanov.bookstore.datasource.IDataSource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.stream.Stream;

public class StreamedAdapter<T> implements IDataAdapter<T> {

    private final IDataSource dataSource;
    private final IDataMapper<T> mapper;

    public StreamedAdapter(IDataSource dataSource, IDataMapper<T> mapper) {
        this.dataSource = dataSource;
        this.mapper = mapper;
    }

    @Override
    public IDataAdapter<T> open() throws FileNotFoundException, IOException {
        dataSource.open();
        return this;
    }

    @Override
    public Stream<T> source() {
        return dataSource
                .load()
                .map(mapper::toModel);
    }

    @Override
    public void close() throws IOException {
        dataSource.close();
    }

}