package com.assanov.bookstore.orm.deserializers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

public class IDataMapperTest {

    IDataMapper mapper = Mockito.mock(IDataMapper.class, Mockito.CALLS_REAL_METHODS);

    @Test
    void shouldReturnEmptyArrayIfNull() {
        String[] arr = mapper.split(null, ",");

        assertEquals(0, arr.length);
    }

    @Test
    void shouldReturnEmptyArrayIfEmpty() {
        String[] arr = mapper.split("   ", ",");

        assertEquals(0, arr.length);
    }

    @Test
    void shouldSplitAndTrimSuccessfully() {
        String[] arr = mapper.split(" Honoré de Balzac , Oscar Wilde   ,William Sydney Porter  ", ",");

        assertEquals(3, arr.length);
        assertArrayEquals(new String[]{"Honoré de Balzac", "Oscar Wilde", "William Sydney Porter"}, arr);
    }

    @Test
    void shouldReturnNullIfNullFieldsArray() {
        String field = mapper.get(null, 2);

        assertNull(field);
    }

    @Test
    void shouldReturnNullIfIndexOutOfBounds() {
        String field = mapper.get(new String[]{"a", "b"}, 2);

        assertNull(field);
    }

    @Test
    void shouldReturnTrimmedFieldIfIndexInBounds() {
        String field = mapper.get(new String[]{"  a ", " b "}, 1);

        assertEquals("b", field);
    }

}
