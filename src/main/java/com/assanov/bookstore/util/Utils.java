package com.assanov.bookstore.util;

import java.util.Collection;
import java.util.StringJoiner;

public class Utils {

    public static String joinValues(Collection<String> values) {
        StringJoiner join = new StringJoiner(", ");
        values.forEach(join::add);
        return join.toString();
    }

    public static String safeArg(String[] args, int index) {
        if (index >= args.length)
            return null;

        return args[index];
    }

}
