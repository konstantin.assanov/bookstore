package com.assanov.bookstore.orm.deserializers;

import com.assanov.bookstore.model.Author;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AuthorMapperTest {

    @Test
    void shouldReturnAuthorSuccessfully() {
        Author author = new AuthorMapper()
                .toModel(new String[]{"email", "firstname", "surname"});

        assertNotNull(author);

        assertEquals("email", author.getEmail());
        assertEquals("firstname", author.getFirstName());
        assertEquals("surname", author.getSurname());
    }

    @Test
    void shouldReturnAuthorSuccessfullyIfEmptyResultSet() {
        Author author = new AuthorMapper()
                .toModel(new String[]{});

        assertNotNull(author);

        assertNull(author.getEmail());
        assertNull(author.getFirstName());
        assertNull(author.getSurname());
    }

}
