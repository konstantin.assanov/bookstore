package com.assanov.bookstore.datasource;

public class DataSourceConfig {

    public final String path;
    public final String delimiter;
    public final boolean isSkipHeader;

    public DataSourceConfig(String path, String delimiter, boolean isSkipHeader) {
        this.path = path;
        this.delimiter = delimiter;
        this.isSkipHeader = isSkipHeader;
    }

}